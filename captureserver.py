from __future__ import print_function
import ctypes
import socket
import time
import struct
import atexit

try:
    from config import path_acapture
except ImportError:
    path_acapture = '/path/to/acapture.so'

try:
    from config import path_encodejpeg
except ImportError:
    path_encodejpeg = '/path/to/encodejpeg.so'

class AndroidCapture(object):
    def __init__(self, acapture_lib):
        self.lib = acapture_lib

        # get screen dimensions
        self.dim = ctypes.cast(ctypes.create_string_buffer(20),\
                                        ctypes.POINTER(ctypes.c_int))
        self.dsz = ctypes.cast(ctypes.create_string_buffer(8),\
                                        ctypes.POINTER(ctypes.c_size_t))
        self.lib.get_screen(self.dim, self.dsz, None, 0)

        self.width, self.height = self.dim[0], self.dim[1]
        self.stride, self.fmt = self.dim[2], self.dim[3]
        self.size = self.dsz[0]

        # create screen buffer
        self.buf = ctypes.create_string_buffer(self.size)
        self.cbuf = ctypes.cast(self.buf, ctypes.POINTER(ctypes.c_ubyte))

    def capture(self, skip=1):
        self.lib.get_screen(self.dim, self.dsz, self.cbuf, skip)
        buflen = self.size/skip
        return self.buf[:buflen]

def closeconn(conn, soc):
    print("Closing connection.")
    conn.close()
    soc.close()

def server(port_num, debug=False):
    try:
        capture = ctypes.cdll.LoadLibrary(path_acapture)
    except OSError:
        print("Can't load library", path_acapture)
        exit()
    try:
        encodejpeg = ctypes.cdll.LoadLibrary(path_encodejpeg)
    except OSError:
        encodejpeg = None
        print("Can't load library", path_encodejpeg, "- encoding disabled")


    # start server
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    soc.bind(('', port_num))
    soc.listen(1)
    print("Waiting for client...")
    conn, addr = soc.accept()
    atexit.register(closeconn, conn, soc)
    print("Connected by", addr[0])

    androidcap = AndroidCapture(capture)

    if encodejpeg:
        jpg = ctypes.create_string_buffer(androidcap.size)
        jpg_c = ctypes.cast(jpg, ctypes.POINTER(ctypes.c_ubyte))

    if debug:
        msec_p2 = int(round(time.time()*1000))

    while True:
        # get 'READY' from client
        data = conn.recv(5)
        if not data: break

        if debug:
            msec_p1 = int(round(time.time()*1000))
            print("Transfer time", msec_p1 - msec_p2, "msec")

        skip = 4
        scr = androidcap.capture(skip)

        if debug:
            msec_p2 = int(round(time.time()*1000))
            print("Capture time", msec_p2 - msec_p1, "msec")

        if encodejpeg != None:
            # only support formats 1, 2, 3 for now (RGBA, 32bit RGB, 24bit RGB)
            # see sPixelformatInfos in ui/PixelFormat.cpp
            if debug:
                msec_p1 = int(round(time.time()*1000))
            if androidcap.fmt in [1, 2]:
                ret = encodejpeg.encodejpeg(\
                            androidcap.width, androidcap.height/skip,\
                            androidcap.stride, androidcap.cbuf, jpg_c, 1)
                scr = jpg[:ret]
            elif androidcap.fmt in [3]:
                ret = encodejpeg.encodejpeg(\
                            androidcap.width, androidcap.height/skip,\
                            androidcap.stride, androidcap.cbuf, jpg_c, 0)
                scr = jpg[:ret]
            else:
                print("Unsupported format, encoding disabled")
                encodejpeg = None
            if debug:
                msec_p2 = int(round(time.time()*1000))
                print("Encode time", msec_p2 - msec_p1, "msec")

        print("Sending", len(scr), "bytes")
        conn.sendall(struct.pack('!i', len(scr)) + scr)
    conn.close()

if __name__ == '__main__':
    import sys
    if(len(sys.argv)) < 2:
        print("Usage: python", sys.argv[0], "[PORT] [-d]")
        exit()
    port_num = int(sys.argv[1])
    debug = False
    if len(sys.argv) > 2 and sys.argv[2] == "-d":
        debug = True
    server(port_num, debug)
