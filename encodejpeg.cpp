#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <jpeglib.h>

void init_buffer(jpeg_compress_struct* cinfo) {}
boolean empty_buffer(jpeg_compress_struct* cinfo) { return TRUE; }
void term_buffer(jpeg_compress_struct* cinfo) {}

#ifdef TEST_LIBJPEG_VERSION
void
test_jpeg_version(j_common_ptr cinfo)
{
    int jpeg_version = cinfo->err->msg_parm.i[0];
    printf("Version: %d\n", jpeg_version);
}
#endif

extern "C" int
encodejpeg(int width, int height, int stride,\
               unsigned char* inbuf, unsigned char* outbuf, int conv_rgba2rgb)
{
    struct jpeg_compress_struct cinfo;
    struct jpeg_error_mgr jerr;
    struct jpeg_destination_mgr dmgr;

#ifdef TEST_LIBJPEG_VERSION
    struct jpeg_decompress_struct d_cinfo;
    struct jpeg_error_mgr d_jerr;

    d_jerr.error_exit = &test_jpeg_version;
    d_cinfo.err = &d_jerr;
    jpeg_CreateDecompress(&d_cinfo, -1, sizeof(cinfo));
#endif

    if(conv_rgba2rgb != 0) {
        /* RGBA -> RGB, inplace */
        int i, j;
        unsigned char d;
        for(j = 0; j < height; j++) {
            for(i = 0; i < width; i++) {
                inbuf[j*width*3+i*3+0] = inbuf[j*stride*4+i*4+0];
                inbuf[j*width*3+i*3+1] = inbuf[j*stride*4+i*4+1];
                inbuf[j*width*3+i*3+2] = inbuf[j*stride*4+i*4+2];
            }
        }
    }

    dmgr.init_destination = init_buffer;
    dmgr.empty_output_buffer = empty_buffer;
    dmgr.term_destination = term_buffer;

    dmgr.next_output_byte = outbuf;
    dmgr.free_in_buffer = width*height*3;

    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_compress(&cinfo);
    cinfo.dest = &dmgr;

    cinfo.image_width = width;
    cinfo.image_height = height;
    cinfo.input_components = 3;
    cinfo.in_color_space = JCS_RGB;

    jpeg_set_defaults(&cinfo);
    jpeg_set_quality(&cinfo, 75, true);
    jpeg_start_compress(&cinfo, true);

    JSAMPROW row_pointer;
    while(cinfo.next_scanline < cinfo.image_height) {
        row_pointer = (JSAMPROW) &inbuf[cinfo.next_scanline * width * 3];
        jpeg_write_scanlines(&cinfo, &row_pointer, 1);
    }
    jpeg_finish_compress(&cinfo);
    return cinfo.dest->next_output_byte - outbuf;
}
