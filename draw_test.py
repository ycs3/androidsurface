import ctypes
import time
from readbmp import BMPFile
try:
    from config import path_asurface
except ImportError:
    path_asurface = '/path/to/asurface.so'

def main():
    bmp = BMPFile('test.bmp')
    asurf = ctypes.cdll.LoadLibrary(path_asurface)
    s = asurf.openSurface("hello", 640, 640, 1000000)
    asurf.fillSurface(s, 0x00, 0x00, 0xFF)
    time.sleep(2)
    asurf.blitSurfaceBGR(s, bmp.data, bmp.width, bmp.height, 0, 0)
    time.sleep(2)
    #asurf.resizeSurface(s, 240, 320)
    #asurf.fillSurface(s, 0xFFF)
    asurf.closeSurface(s)

if __name__ == '__main__':
    main()
