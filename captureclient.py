from __future__ import print_function
import ctypes
import socket
import struct
import atexit
import numpy
import cv2
from cStringIO import StringIO
from PIL import Image

def closeconn(conn):
    print("Closing connection.")
    conn.close()

def client(remote_addr, remote_port, use_jpeg):
    # start client
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    soc.connect((remote_addr, remote_port))
    atexit.register(closeconn, soc)

    while True:
        soc.sendall("READY")
        recv_len = struct.unpack('!i', soc.recv(4))[0]
        data = bytearray(recv_len)
        view = memoryview(data)
        while recv_len:
            nb = soc.recv_into(view, recv_len)
            view = view[nb:]
            recv_len -= nb

        if use_jpeg:
            im = Image.open(StringIO(data))
            k = numpy.asarray(im)[:,:,::-1].copy()
        else:
            nbuf = numpy.frombuffer(data, dtype=numpy.uint8)
            w = 768
            h = (nbuf.shape[0]/w)/4
            im = nbuf.reshape((h, w, 4))
            k = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        k = cv2.resize(k, (768, 1280))
        #k = cv2.transpose(k)
        #k = cv2.flip(k, 0)
        cv2.imshow('Android', k)
        cv2.waitKey(10)

if __name__ == '__main__':
    import sys

    if(len(sys.argv)) < 3:
        print("Usage: python", sys.argv[0], "[remote_addr] [remote_port] [jpg]")
        exit()
    remote_addr = sys.argv[1]
    remote_port = int(sys.argv[2])
    use_jpeg = False
    if len(sys.argv) > 3:
        use_jpeg = True
    client(remote_addr, remote_port, use_jpeg)
