import sys
import ctypes
import time

try:
    from config import path_encodejpeg
except ImportError:
    path_encodejpeg = "/path/to/encodjpeg.so"

def test(filename, w, h, s):
    encodejpeg = ctypes.cdll.LoadLibrary(path_encodejpeg)

    with open(filename, 'r') as fi:
        raw = fi.read()
    print "File length:", len(raw)
    d = len(raw) / (s*h)
    print "wt:", w, "ht:", h, "stride:", s
    print "Calculated channels:", d
    print "stride * ht * channels:", s * h * d

    raw_c = ctypes.cast(raw, ctypes.POINTER(ctypes.c_ubyte))

    jpg = ctypes.create_string_buffer(w*h*d) # max size
    jpg_c = ctypes.cast(jpg, ctypes.POINTER(ctypes.c_ubyte))

    t1 = time.time()
    if d == 4: # assumes RGBA
        ret = encodejpeg.encodejpeg(w, h, s, raw_c, jpg_c, 1)
    elif d == 3: # assumes RGB
        ret = encodejpeg.encodejpeg(w, h, s, raw_c, jpg_c, 0)
    else:
        "Error: Unknown number of channels"
        return
    t2 = time.time()

    print "Bytes written:", ret, "in", t2-t1, "sec"

    with open(filename + ".jpg", 'wb') as f:
        f.write(jpg[:ret])

if __name__ == '__main__':
    if len(sys.argv) != 5:
        print "Usage: python", sys.argv[0], "[raw file] [width] [height] [stride]"
        print "raw file captured using screencap"
        exit()
    test(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]))
