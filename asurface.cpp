#include <cutils/memory.h>
#include <utils/Log.h>

#include <binder/IPCThreadState.h>
#include <binder/ProcessState.h>
#include <binder/IServiceManager.h>

#include <gui/Surface.h>
#include <gui/SurfaceComposerClient.h>

#define GETP_VAL(offset, width, x, y, ch)\
            (*((offset) + (width) * 3 * (y) + (x) * 3 + (ch)))
#define GETP_RGB(offset, width, x, y)\
            (((GETP_VAL(offset, width, x, y, 0) & 0xf8) << 8) | \
             ((GETP_VAL(offset, width, x, y, 1) & 0xfc) << 3) | \
             ((GETP_VAL(offset, width, x, y, 2) & 0xf8) >> 3))
#define GETP_BGR(offset, width, x, y)\
            (((GETP_VAL(offset, width, x, y, 2) & 0xf8) << 8) | \
             ((GETP_VAL(offset, width, x, y, 1) & 0xfc) << 3) | \
             ((GETP_VAL(offset, width, x, y, 0) & 0xf8) >> 3))
#define CONV_RGB(r, g, b)\
            ((((r) & 0xf8) << 8) | (((g) & 0xfc) << 3) | (((b) & 0xf8) >> 3))

using namespace android;

typedef struct Raw_ASurface {
    sp<SurfaceComposerClient> client;
    sp<SurfaceControl> surfaceControl;
    sp<Surface> surface;
} ASurface;

extern "C" void*
createASurface(void)
{
    ASurface* asurface;
    if((asurface = (ASurface*)calloc(1, sizeof(ASurface))) == NULL) {
        return NULL;
    }
    return (void*)asurface;
}

extern "C" void
deleteASurface(void* _asurface)
{
    ASurface* asurface = (ASurface*)_asurface;

    sp<SurfaceComposerClient> client = asurface->client;
    client->dispose();
    free(asurface);
}

extern "C" void
getSurfaceComposerClient(void* _asurface)
{
    ASurface* asurface = (ASurface*)_asurface;

    sp<SurfaceComposerClient> client = new SurfaceComposerClient();
    asurface->client = client;
}

extern "C" void
getSurfaceControl(void* _asurface, const char* name, int width, int height)
{
    ASurface* asurface = (ASurface*)_asurface;

    sp<SurfaceComposerClient> client = asurface->client;
    sp<SurfaceControl> surfaceControl = client->createSurface(String8(name), width, height, PIXEL_FORMAT_RGB_565, 0);
    asurface->surfaceControl = surfaceControl;
}

extern "C" void
setSurfaceControlLayer(void* _asurface, int layer)
{
    ASurface* asurface = (ASurface*)_asurface;

    sp<SurfaceControl> surfaceControl = asurface->surfaceControl;
    SurfaceComposerClient::openGlobalTransaction();
    surfaceControl->setLayer(layer);
    SurfaceComposerClient::closeGlobalTransaction();
}

extern "C" void
getSurface(void* _asurface)
{
    ASurface* asurface = (ASurface*)_asurface;

    sp<SurfaceControl> surfaceControl = asurface->surfaceControl;
    sp<Surface> surface = surfaceControl->getSurface();
    asurface->surface = surface;
}

extern "C" void
fillSurface_raw(void* _asurface, uint16_t color)
{
    ASurface* asurface = (ASurface*)_asurface;

    sp<Surface> surface = asurface->surface;
    ANativeWindow_Buffer outBuffer;
    ssize_t bpr;
    surface->lock(&outBuffer, NULL);
    bpr = outBuffer.stride * bytesPerPixel(outBuffer.format);
    android_memset16((uint16_t*)outBuffer.bits, color, bpr*outBuffer.height);
    surface->unlockAndPost();
}

extern "C" void
fillSurface(void* _asurface, int r, int g, int b)
{
    ASurface* asurface = (ASurface*)_asurface;
    uint16_t color = CONV_RGB(r, g, b);

    sp<Surface> surface = asurface->surface;
    ANativeWindow_Buffer outBuffer;
    ssize_t bpr;
    surface->lock(&outBuffer, NULL);
    bpr = outBuffer.stride * bytesPerPixel(outBuffer.format);
    android_memset16((uint16_t*)outBuffer.bits, color, bpr*outBuffer.height);
    surface->unlockAndPost();
}

extern "C" void
resizeSurface(void* _asurface, int width, int height)
{
    ASurface* asurface = (ASurface*)_asurface;

    sp<SurfaceControl> surfaceControl = asurface->surfaceControl;
    SurfaceComposerClient::openGlobalTransaction();
    surfaceControl->setSize(width, height);
    SurfaceComposerClient::closeGlobalTransaction();
}

extern "C" void
blitSurface_raw(void* _asurface, uint16_t* buf, int wt, int ht, int ox, int oy)
{
    ASurface* asurface = (ASurface*)_asurface;

    sp<Surface> surface = asurface->surface;
    ANativeWindow_Buffer outBuffer;
    ssize_t bpp, bpr, blit_bpr;
    int j;
    uint16_t* dest;

    surface->lock(&outBuffer, NULL);
    bpp = bytesPerPixel(outBuffer.format);
    bpr = outBuffer.stride * bytesPerPixel(outBuffer.format);
    blit_bpr = wt * bytesPerPixel(outBuffer.format);
    for(j = 0; j < ht; j++) {
        dest = (uint16_t*)((uint8_t*)outBuffer.bits+((oy+j)*bpr)+ox*bpp);
        memcpy(dest, buf, blit_bpr);
    }
    surface->unlockAndPost();
}

extern "C" void
blitSurface(void* _asurface, const char* buf, int wt, int ht, int ox, int oy)
{
    ASurface* asurface = (ASurface*)_asurface;

    sp<Surface> surface = asurface->surface;
    ANativeWindow_Buffer outBuffer;
    ssize_t bpp, bpr, blit_bpr;
    int i, j;
    uint16_t* dest;

    surface->lock(&outBuffer, NULL);
    bpp = bytesPerPixel(outBuffer.format);
    bpr = outBuffer.stride * bytesPerPixel(outBuffer.format);
    blit_bpr = wt * bytesPerPixel(outBuffer.format);
    for(j = 0; j < ht; j++) {
        for(i = 0; i < wt; i++) {
            dest = (uint16_t*)((uint8_t*)outBuffer.bits+((oy+j)*bpr)+(ox+i)*bpp);
            *dest = GETP_RGB(buf, wt, i, j);
        }
    }
    surface->unlockAndPost();
}

extern "C" void
blitSurfaceBGR(void* _asurface, const char* buf, int wt, int ht, int ox, int oy)
{
    ASurface* asurface = (ASurface*)_asurface;

    sp<Surface> surface = asurface->surface;
    ANativeWindow_Buffer outBuffer;
    ssize_t bpp, bpr, blit_bpr;
    int i, j;
    uint16_t* dest;

    surface->lock(&outBuffer, NULL);
    bpp = bytesPerPixel(outBuffer.format);
    bpr = outBuffer.stride * bytesPerPixel(outBuffer.format);
    blit_bpr = wt * bytesPerPixel(outBuffer.format);
    for(j = 0; j < ht; j++) {
        for(i = 0; i < wt; i++) {
            dest = (uint16_t*)((uint8_t*)outBuffer.bits+((oy+j)*bpr)+(ox+i)*bpp);
            *dest = GETP_BGR(buf, wt, i, j);
        }
    }
    surface->unlockAndPost();
}

extern "C" void*
openSurface(const char* name, int width, int height, int layer)
{
    void* asurface = createASurface();
    getSurfaceComposerClient(asurface);
    getSurfaceControl(asurface, name, width, height);
    setSurfaceControlLayer(asurface, layer);
    getSurface(asurface);
    return asurface;
}

extern "C" void
closeSurface(void* asurface)
{
    deleteASurface(asurface);
}
