import struct

class BMPFile:
    def __init__(self, filename):
        self.valid = False

        filedata = None
        with open(filename) as f:
            filedata = f.read()
        if filedata[0:2] != "BM":
            print "Error: Not a BMP file"
            return
        # Little-endian standard size
        # WORD=2,DWORD/LONG=4 : H=2,I=4
        s = struct.unpack("<I2H4I2H6I", filedata[2:54])
        hdr_keys = ['fsize', 'res1', 'res2', 'offset', 'size',\
                    'width', 'height', 'planes', 'bit_count',\
                    'compression', 'imagesize', 'xres', 'yres',\
                    'ncolors', 'impcolors']
        self.hdr = dict()
        for k in hdr_keys:
            self.hdr[k] = s[hdr_keys.index(k)]
        if self.hdr['compression'] != 0 or self.hdr['bit_count'] != 24:
            print "Error: BMP needs to be uncompressed, 24bit"
            return

        self.width = self.hdr['width']
        self.height = self.hdr['height']
        self.data = "" # data is BGR

        # 32 bit boundary for each line
        line_len = self.hdr['width'] * 3
        if line_len % 4:
            line_len += (4 - (line_len % 4))
        data_size = line_len * self.hdr['height']
        offset = self.hdr['offset'] + data_size - line_len
        for j in range(self.hdr['height']):
            self.data += filedata[offset:offset+self.hdr['width']*3]
            offset -= line_len
        self.valid = True

    def get_pixel_rgb(self, x, y):
        i = y * self.width * 3 + x * 3
        return ord(self.data[i+2]), ord(self.data[i+1]), ord(self.data[i])

if __name__ == '__main__':
    import cv2
    import numpy

    bmp = BMPFile('test.bmp')
    a = numpy.zeros((bmp.height, bmp.width, 3), dtype=numpy.uint8)
    for j in range(bmp.height):
        for i in range(bmp.width):
            r, g, b = bmp.get_pixel_rgb(i, j)
            a[j][i][0] = b
            a[j][i][1] = g
            a[j][i][2] = r
    cv2.imshow('IM1', a)

    b = numpy.fromstring(bmp.data, dtype=numpy.uint8)
    b.shape = (bmp.height, bmp.width, 3)
    cv2.imshow('IM2', b)
    cv2.waitKey()

