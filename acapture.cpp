#include <stdio.h>

#include <gui/SurfaceComposerClient.h>
#include <gui/ISurfaceComposer.h>

#include <ui/PixelFormat.h>

/* android 4.3 see https://code.google.com/p/android/issues/detail?id=59649 */

using namespace android;

static uint32_t DEFAULT_DISPLAY_ID = ISurfaceComposer::eDisplayIdMain;

ScreenshotClient screenshot;
sp<IBinder> display = SurfaceComposerClient::getBuiltInDisplay(DEFAULT_DISPLAY_ID);

extern "C" int
get_screen(uint32_t* a, size_t* size, uint8_t* d, int skip)
{
    void const* base = 0;
    uint32_t w, s, h, f;
    int ret = -1;

    if(display != NULL) {
        ret = screenshot.update(display);
        if(ret == NO_ERROR) {
            base = screenshot.getPixels();
            w = screenshot.getWidth();
            h = screenshot.getHeight();
            s = screenshot.getStride();
            f = screenshot.getFormat();
            *size = screenshot.getSize();
            a[0] = w; a[1] = h; a[2] = s; a[3] = f;
#if 0
            printf("w:%d, h:%d, s:%d, f:%d, sz: %d, d: %d\n",
                                        w, h, s, f, *size, d);
#endif
            if(d != 0) {
                size_t bpp = bytesPerPixel(f);
                uint8_t* dst_ptr = d;
                
                for(size_t y = 0; y < h; y = y + skip) {
                    memcpy(dst_ptr, base, w*bpp);
                    dst_ptr = dst_ptr + s*bpp;
                    base = (void *)((char *)base + s*bpp*skip);
                }
            }
        }
    }
    return ret;
}
